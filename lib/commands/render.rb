require './lib/command'

class RenderCommand < Command
  def execute
    raise Bump::NoBitmap unless @app.bitmap
    puts @app.bitmap.data.to_a.map{ |p| p.map(&:color).join('') }
  end
end