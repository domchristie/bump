require './lib/command'

class DrawHorizontalCommand < Command
  def execute(x1, x2, y, color)
    raise Bump::NoBitmap unless @app.bitmap

    unless @app.bitmap.pixel_at(x1, y) && @app.bitmap.pixel_at(x2, y)
      raise Bump::CoordinatesOutOfRange
    end

    @app.bitmap.pixels_between(x1, y, x2, y).each { |p| p.color = color }
  end
end