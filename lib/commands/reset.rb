require './lib/command'

class ResetCommand < Command
  def execute
    raise Bump::NoBitmap unless @app.bitmap
    @app.bitmap.reset
  end
end