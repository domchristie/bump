require './lib/command'
require './lib/error'

class ColorPixelCommand < Command
  def execute(x, y, color)
    raise Bump::NoBitmap unless @app.bitmap

    pixel = @app.bitmap.pixel_at(x, y)
    raise Bump::CoordinatesOutOfRange unless pixel

    @app.bitmap.pixel_at(x, y).color = color
  end
end