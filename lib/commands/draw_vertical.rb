require './lib/command'

class DrawVerticalCommand < Command
  def execute(x, y1, y2, color)
    raise Bump::NoBitmap unless @app.bitmap

    unless @app.bitmap.pixel_at(x, y1) && @app.bitmap.pixel_at(x, y2)
      raise Bump::CoordinatesOutOfRange
    end

    @app.bitmap.pixels_between(x, y1, x, y2).each { |p| p.color = color }
  end
end