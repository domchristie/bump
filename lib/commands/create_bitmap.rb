require './lib/command'
require './lib/bitmap'

class CreateBitmapCommand < Command
  def execute(width, height)
    @app.bitmap = Bitmap.new(width, height)
  end
end