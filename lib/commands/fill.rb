require './lib/command'

class FillCommand < Command
  def execute(x, y, color)
    raise Bump::NoBitmap unless @app.bitmap

    pixel = @app.bitmap.pixel_at(x, y)
    raise Bump::CoordinatesOutOfRange unless pixel

    flood_fill(x, y, @app.bitmap.pixel_at(x, y).color, color)
  end

private

  def flood_fill(x, y, target_color, replacement_color)
    return if target_color == replacement_color

    return unless pixel = @app.bitmap.pixel_at(x, y)
    return if pixel.color != target_color
    return if pixel.color == replacement_color

    pixel.color = replacement_color

    flood_fill(x-1, y, target_color, replacement_color)
    flood_fill(x+1, y, target_color, replacement_color)
    flood_fill(x, y-1, target_color, replacement_color)
    flood_fill(x, y+1, target_color, replacement_color)
  end
end