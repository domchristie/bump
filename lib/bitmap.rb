require 'matrix'

require './lib/pixel'
require './lib/error'

class Bitmap
  attr_reader :width, :height, :data

  MIN_WIDTH = 1
  MAX_WIDTH = 250
  MIN_HEIGHT = 1
  MAX_HEIGHT = 250

  def initialize(width, height)
    self.width = width
    self.height = height
    reset
  end

  def reset
    @data = Matrix.build(height, width){ Pixel.new }
  end

  def pixel_at(x, y)
    @data[y, x] if coordinates_in_range?(x, y)
  end

  def pixels_between(x1, y1, x2, y2)
    @data.minor(y1..y2, x1..x2).to_a.flatten
  end

private

  def coordinates_in_range?(x, y)
    (0..width).include?(x) && (0..height).include?(y)
  end

  def width=(width)
    raise Bump::InvalidWidth unless (MIN_WIDTH..MAX_WIDTH).include?(width)
    @width = width
  end

  def height=(height)
    raise Bump::InvalidHeight unless (MIN_HEIGHT..MAX_HEIGHT).include?(height)
    @height = height
  end
end