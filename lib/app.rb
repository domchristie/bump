require 'singleton'
require './lib/command_parser'
require './lib/error'

class App
  include Singleton
  attr_accessor :bitmap

  def run
    catch :exit do
      while input = get_input
        begin
        command, args = parse_input(input)
        command.new(self).execute(*args)
        rescue Bump::Error => e
          puts "Oops! #{e.message}"
        end
      end
    end
  end

private

  def parse_input(input)
    CommandParser.instance.parse(input)
  end

  def get_input
    print '> '
    $stdin.gets
  end
end