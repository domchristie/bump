require 'singleton'
Dir['./lib/commands/*.rb'].each {|file| require file }
require './lib/error'

class CommandParser
  include Singleton

  def parse(input)
    parts = input.split(' ')
    command = parts.shift

    case command
    when 'I'
      [CreateBitmapCommand, parts.map{ |d| d.to_i }]
    when 'C'
      [ResetCommand]
    when 'L'
      [ColorPixelCommand, zero_base_coordinates(parts[0..1]) << parts[2]]
    when 'H'
      [DrawHorizontalCommand, zero_base_coordinates(parts[0..2]) << parts[3]]
    when 'V'
      [DrawVerticalCommand, zero_base_coordinates(parts[0..2]) << parts[3]]
    when 'F'
      [FillCommand, zero_base_coordinates(parts[0..1]) << parts[2]]
    when 'S'
      [RenderCommand]
    when 'X'
      throw :exit
    else
      raise Bump::InvalidCommand
    end
  end

private

  def zero_base_coordinates(coords)
    coords.map { |a| a.to_i - 1 }
  end
end