require './lib/bitmap'

module Bump
  class Error < StandardError; end

  class InvalidColor < Error
    def message
      'Color needs to be a single letter.'
    end
  end

  class InvalidWidth < Error
    def message
      "Width needs to be between #{Bitmap::MIN_WIDTH}-#{Bitmap::MAX_WIDTH}."
    end
  end

  class InvalidHeight < Error
    def message
      "Height needs to be between #{Bitmap::MIN_HEIGHT}-#{Bitmap::MAX_HEIGHT}."
    end
  end

  class InvalidCommand < Error
    def message
      'Unknown command.'
    end
  end

  class NoBitmap < Error
    def message
      'First create a bitmap using: I X Y'
    end
  end

  class CoordinatesOutOfRange < Error
    def message
      'Coordinates are out of range.'
    end
  end
end