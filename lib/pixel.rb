require './lib/error'

class Pixel
  DEFAULT_COLOR = 'O'
  attr_accessor :color

  def initialize(color=DEFAULT_COLOR)
    self.color = color
  end

  def color=(color)
    raise Bump::InvalidColor unless color =~ /^[a-z]$/i
    @color = color.upcase
  end
end