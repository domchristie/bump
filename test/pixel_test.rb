require 'pixel'
require 'error'

class PixelTest < Minitest::Test
  def test_default_color
    assert_equal Pixel::DEFAULT_COLOR, Pixel.new.color
  end

  def test_initialization_with_color
    assert_equal 'J', Pixel.new('J').color
  end

  def test_initialization_with_lowercase
    assert_equal 'J', Pixel.new('j').color
  end

  def test_initialization_with_invalid
    assert_raises Bump::InvalidColor do
      Pixel.new(1)
    end
  end
end