require 'minitest/autorun'
require 'matrix'
require 'bitmap'
require 'error'

class BitmapTest < Minitest::Test
  def setup
    @width = 5
    @height = 6
    @bmp = Bitmap.new(@width, @height)

    @matrix_stub = Matrix[['A', 'B', 'C'],
                          ['D', 'E', 'F'],
                          ['G', 'H', 'I']]
  end

  def test_width
    assert_equal @width, @bmp.width
  end

  def test_height
    assert_equal @height, @bmp.height
  end

  def test_invalid_width
    assert_raises Bump::InvalidWidth do
      Bitmap.new(Bitmap::MAX_WIDTH+1, 1)
    end
  end

  def test_invalid_height
    assert_raises Bump::InvalidHeight do
      Bitmap.new(1, Bitmap::MIN_HEIGHT-1)
    end
  end

  def test_pixel_at
    Matrix.stub :build, @matrix_stub do
      bmp = Bitmap.new(3, 3)
      assert_equal 'A', bmp.pixel_at(0, 0)
      assert_equal 'B', bmp.pixel_at(1, 0)
      assert_equal 'C', bmp.pixel_at(2, 0)
      assert_equal 'D', bmp.pixel_at(0, 1)
      assert_equal 'E', bmp.pixel_at(1, 1)
      assert_equal 'F', bmp.pixel_at(2, 1)
      assert_equal 'G', bmp.pixel_at(0, 2)
      assert_equal 'H', bmp.pixel_at(1, 2)
      assert_equal 'I', bmp.pixel_at(2, 2)
    end
  end

  def test_pixel_at_out_of_range
    assert_nil @bmp.pixel_at(-1, 0)
    assert_nil @bmp.pixel_at(0, -1)
    assert_nil @bmp.pixel_at(999, -1)
  end

  def test_pixels_between
    Matrix.stub :build, @matrix_stub do
      bmp = Bitmap.new(3, 3)
      assert_equal ['A', 'D', 'G'], bmp.pixels_between(0, 0, 0, 2)
      assert_equal ['B', 'E'], bmp.pixels_between(1, 0, 1, 1)
      assert_equal ['A', 'B', 'C'], bmp.pixels_between(0, 0, 2, 0)
      assert_equal ['H', 'I'], bmp.pixels_between(1, 2, 2, 2)
    end
  end

  def test_reset
    mock = MiniTest::Mock.new
    mock.expect(:call, nil, [@height, @width])

    Matrix.stub :build, mock do
      @bmp.reset
    end

    assert mock.verify
  end
end
