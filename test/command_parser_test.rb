require 'command_parser'
Dir['commands/*.rb'].each {|file| require file }
require 'error'

class CommandParserTest < Minitest::Test
  def test_parse_I
    assert_equal [CreateBitmapCommand, [5, 6]],
      CommandParser.instance.parse('I 5 6')
  end

  def test_parse_C
    assert_equal [ResetCommand], CommandParser.instance.parse('C')
  end

  def test_parse_L
    assert_equal [ColorPixelCommand, [1, 2, 'A']],
      CommandParser.instance.parse('L 2 3 A')
  end

  def test_parse_H
    assert_equal [DrawHorizontalCommand, [2, 3, 1, 'Z']],
      CommandParser.instance.parse('H 3 4 2 Z')
  end

  def test_parse_V
    assert_equal [DrawVerticalCommand, [1, 2, 3, 'W']],
      CommandParser.instance.parse('V 2 3 4 W')
  end

  def test_parse_F
    assert_equal [RenderCommand], CommandParser.instance.parse('S')
  end

  def test_parse_X
    assert_throws :exit do
      CommandParser.instance.parse('X')
    end
  end

  def test_parse_invalid
    assert_raises Bump::InvalidCommand do
      CommandParser.instance.parse('4567')
    end
  end
end