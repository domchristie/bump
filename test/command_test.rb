require 'matrix'
require 'app'
require 'bitmap'
Dir['commands/*.rb'].each {|file| require file }
require 'error'

class CommandTest < Minitest::Test
  def setup
    @app = App.instance
    @width = 5
    @height = 6
    @bmp = Bitmap.new(@width, @height)
  end

  def test_create_bitmap
    CreateBitmapCommand.new(@app).execute(@width, @height)
    assert @app.bitmap
    assert_equal @width, @app.bitmap.width
    assert_equal @height, @app.bitmap.height
  end

  def test_render
    @app.stub :bitmap, @bmp do
      out, err = capture_io do
        RenderCommand.new(@app).execute
      end

      assert /(#{Pixel::DEFAULT_COLOR}{#{@width}}\n?){#{@height}}/ =~ out
    end
  end

  def test_reset
    bmp = MiniTest::Mock.new
    bmp.expect(:reset, nil)

    @app.stub :bitmap, bmp do
      ResetCommand.new(@app).execute
    end

    assert bmp.verify
  end

  def test_color_pixel
    @app.stub :bitmap, @bmp do
      ColorPixelCommand.new(@app).execute(0, 0, 'Z')
      assert_equal 'Z', @bmp.pixel_at(0, 0).color
    end
  end

  def test_color_pixel_invalid
    @app.stub :bitmap, @bmp do
      assert_raises Bump::CoordinatesOutOfRange do
        ColorPixelCommand.new(@app).execute(999, 0, 'Z')
      end
    end
  end

  def test_color_pixel_no_bmp
    @app.stub :bitmap, nil do
      assert_raises Bump::NoBitmap do
        ColorPixelCommand.new(@app).execute(0, 0, 'Z')
      end
    end
  end

  def test_draw_horizontal_line
    @app.stub :bitmap, @bmp do
      DrawHorizontalCommand.new(@app).execute(0, 2, 0, 'Z')
      assert_equal 'Z', @bmp.pixel_at(0, 0).color
      assert_equal 'Z', @bmp.pixel_at(1, 0).color
      assert_equal 'Z', @bmp.pixel_at(2, 0).color
    end
  end

  def test_draw_horizontal_invalid
    @app.stub :bitmap, @bmp do
      assert_raises Bump::CoordinatesOutOfRange do
        DrawHorizontalCommand.new(@app).execute(@width+1, 2, 0, 'Z')
      end
    end
  end

  def test_draw_vertical
    @app.stub :bitmap, @bmp do
      DrawVerticalCommand.new(@app).execute(0, 0, 2, 'Z')
      assert_equal 'Z', @bmp.pixel_at(0, 0).color
      assert_equal 'Z', @bmp.pixel_at(0, 1).color
      assert_equal 'Z', @bmp.pixel_at(0, 2).color
    end
  end

  def test_draw_horizontal_invalid
    @app.stub :bitmap, @bmp do
      assert_raises Bump::CoordinatesOutOfRange do
        DrawVerticalCommand.new(@app).execute(0, -1, 2, 'Z')
      end
    end
  end
end